package com.fangdd.esf.hc.thriftimpl;

import com.fangdd.esf.hc.idl.*;
import org.apache.thrift.TException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("houseCenterService")
public class HouseCenterServiceImpl implements HouseCenterService.Iface{

	public AddHouseBasicInfoResp addHouseBasicInfo(
			AddHouseBasicInfoReq houseBasicInfo, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp modifyHouseBasicInfo(ModifyHouseBasicInfoReq modify,
			TCallerInfo callerInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp setHouseOnsaleAndValuated(long houseId,
			int isonsale, int isvaluted, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp setHouseIsDisplayed(long houseId, int isdisplayed,
			long workorderId, TCallerInfo callerInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp setHouseAuditStatus(long houseId, int auditstatus,
			long workorderId, TCallerInfo callerInfo) throws TException {
		System.out.println("method:setHouseAuditStatus.....");
		
		HouseCenterResp resp = new HouseCenterResp(201,"成功调用！");
		return resp;
	}

	public HouseCenterResp setHouseIsDeleted(long houseId, long workorderId,
			TCallerInfo callerInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public BatchQueryHouseBasicInfoResp BatchQueryHouseBasicInfo(
			BatchQueryHouseBasicInfoReq batchQueryReq, TCallerInfo callerInfo,
			PageInfo pageInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp modifyHouseExtendedInfo(
			ModifyHouseExtendedInfoReq modify, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp modifyHouseTagInfo(ModifyHouseTagInfoReq modify,
			TCallerInfo callerInfo) throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public BatchModifyHouseImageResp batchModifyHouseImage(
			List<HouseImage> imageList, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public AddHouseCommentResp addHouseComment(
			AddHouseCommentReq houseCommentReq, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public AddHouseCommentReplyResp addHouseCommentReply(
			AddHouseCommentReplyReq houseCommentReplyReq, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public BatchQueryHouseCommentInfoResp batchQueryHouseCommentInfo(
			List<Long> houseIds, TCallerInfo callerInfo, PageInfo pageInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public HouseCenterResp addHouseAppraisal(
			AddHouseAppraisalReq houseAppraisal, TCallerInfo callerInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

	public BatchQueryHouseAppraisalResp batchQueryHouseAppraisal(
			List<Long> houseIds, TCallerInfo callerInfo, PageInfo pageInfo)
			throws TException {
		// TODO Auto-generated method stub
		return null;
	}

}
