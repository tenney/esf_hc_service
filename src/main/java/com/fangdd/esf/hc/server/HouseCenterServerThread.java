package com.fangdd.esf.hc.server;

import com.fangdd.esf.hc.idl.HouseCenterService;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HouseCenterServerThread extends Thread{

	private static final Logger logger = LoggerFactory.getLogger(HouseCenterServerThread.class);
	
    // 处理接收请求线程数
    private int                      selectorThreadNum = 5;

    // 工作线程数
    private int                      workerThreadNum   = 64;

    // thrift 监听port端口号
    private int                      port;
	
    //房源中心service服务
    private HouseCenterService.Iface houseCenterService;
    
	
	public HouseCenterServerThread(int selectorThreadNum, int workerThreadNum,
			int port) {
		super();
		this.selectorThreadNum = selectorThreadNum;
		this.workerThreadNum = workerThreadNum;
		this.port = port;
	}



	@Override
	public void run() {
		startServer();
	}
	
	/**
	 * 服务端监听启动方法
	 */
	private void startServer(){
        try {
            System.out.println("Thrift listener initialization, port："+port);

            // 对外提供多个接口服务，使用TMultiplexedProcessor包装处理
            TMultiplexedProcessor tprocessor = new TMultiplexedProcessor();
            // 注册接口服务
            registerServices(tprocessor);

            // 非阻塞serversocket方式
            TNonblockingServerSocket tnbSocketTransport = new TNonblockingServerSocket(port);

            // 使用thread线程池+selector/nio方式server端
            TThreadedSelectorServer.Args ttsArgs = new TThreadedSelectorServer.Args(
                    tnbSocketTransport);
            ttsArgs.processor(tprocessor);
            ttsArgs.transportFactory(new TFramedTransport.Factory());
            ttsArgs.protocolFactory(new TCompactProtocol.Factory());

            // 此处需要限制最大readBufferBytes大小,设定为2M
            // 2M = 2 * 1024KB = 2 * 1024 * 1024byte = 2097152bytes
            // telnet localhost port会导致溢出 Java heap dump
            ttsArgs.maxReadBufferBytes = 2 * 1024 * 1024;

            // 设置server接收请求的线程数
            ttsArgs.selectorThreads(selectorThreadNum);
            // 设置server处理的工作线程数
            ttsArgs.workerThreads(workerThreadNum);

            TThreadedSelectorServer server = new TThreadedSelectorServer(ttsArgs);

            logger.info("Thrift listener started.");

            server.serve();

        } catch (Exception e) {
            logger.error("Thrift listener failed to start.", e);
        }
    }
    private void registerServices(TMultiplexedProcessor processor) {
        processor.registerProcessor("HouseCenterService",
                new HouseCenterService.Processor<HouseCenterService.Iface>(houseCenterService));
    }



	public int getSelectorThreadNum() {
		return selectorThreadNum;
	}



	public void setSelectorThreadNum(int selectorThreadNum) {
		this.selectorThreadNum = selectorThreadNum;
	}



	public int getWorkerThreadNum() {
		return workerThreadNum;
	}



	public void setWorkerThreadNum(int workerThreadNum) {
		this.workerThreadNum = workerThreadNum;
	}



	public int getPort() {
		return port;
	}



	public void setPort(int port) {
		this.port = port;
	}



	public HouseCenterService.Iface getHouseCenterService() {
		return houseCenterService;
	}



	public void setHouseCenterService(HouseCenterService.Iface houseCenterService) {
		this.houseCenterService = houseCenterService;
	}
    
	

}
