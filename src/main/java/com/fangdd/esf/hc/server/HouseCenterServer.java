package com.fangdd.esf.hc.server;

import com.fangdd.esf.hc.idl.HouseCenterService;
import com.fangdd.esf.hc.thriftimpl.HouseCenterServiceImpl;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TTransportException;


public class HouseCenterServer {
	
	public static final int SERVER_PORT = 8090;
	
	public void startServer(){
		
		System.out.println("HouseCenter TSimpleServer start...........");
		
		try {
			TProcessor tProcessor = new HouseCenterService.Processor<HouseCenterService.Iface>(new HouseCenterServiceImpl());
			//设置服务器端口
			TServerSocket serverTransport = new TServerSocket(SERVER_PORT);
			TServer.Args tArgs = new TServer.Args(serverTransport);
			tArgs.processor(tProcessor);
			
			//设置协议工厂为TBinaryProtocol.Factory
			//			数据传输协议
			//			TBinaryProtocol : 二进制格式.
			//			TCompactProtocol : 压缩格式
			//			TJSONProtocol : JSON格式
			//			TSimpleJSONProtocol : 提供JSON只写协议, 生成的文件很容易通过脚本语言解析
			tArgs.protocolFactory(new TBinaryProtocol.Factory());
			
			TServer server = new TSimpleServer(tArgs);
			server.serve();

			
		} catch (TTransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String []args){
		HouseCenterServer server = new HouseCenterServer();
		server.startServer();
	}

}
