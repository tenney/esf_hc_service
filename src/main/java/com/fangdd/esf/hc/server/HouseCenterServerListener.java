package com.fangdd.esf.hc.server;

import com.fangdd.esf.hc.idl.HouseCenterService;
import org.springframework.beans.factory.InitializingBean;


public class HouseCenterServerListener implements InitializingBean{

	public HouseCenterServerListener() {
		super();
	}
	
	private int serverPort = 30025;
	
	private int selectorThreadNum = 5;
	
	private int workerThreadNums = 64;
	
	private HouseCenterService.Iface houseCenterService;
	
	public int getServerPort() {
		return serverPort;
	}



	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}



	public int getSelectorThreadNum() {
		return selectorThreadNum;
	}



	public void setSelectorThreadNum(int selectorThreadNum) {
		this.selectorThreadNum = selectorThreadNum;
	}



	public int getWorkerThreadNums() {
		return workerThreadNums;
	}



	public void setWorkerThreadNums(int workerThreadNums) {
		this.workerThreadNums = workerThreadNums;
	}




	public HouseCenterService.Iface getHouseCenterService() {
		return houseCenterService;
	}



	public void setHouseCenterService(HouseCenterService.Iface houseCenterService) {
		this.houseCenterService = houseCenterService;
	}



	public void afterPropertiesSet() throws Exception {
		System.out.println("监听器启动........");
        // init thread pool
        HouseCenterServerThread serverThread = new HouseCenterServerThread(selectorThreadNum, workerThreadNums,serverPort);
        // register service
        serverThread.setHouseCenterService(houseCenterService);
        // set thread name
        serverThread.setName("Thrift_HouseCenter_Server_Thread_001");
        // set daemon serverThread
        serverThread.setDaemon(true);
        serverThread.start();
	}

}
