package com.fangdd.esf.hc.server;

import com.fangdd.esf.hc.utils.HCConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class HouseCenterServerMain {

	private static final Logger logger = LoggerFactory.getLogger(HouseCenterServerMain.class);

	public static final String SHUTDOWN_HOOK_KEY = "server.shutdown.hook";
	
	private static volatile boolean running = true;
	
	public static void main(String [] args){
		
		long start = System.currentTimeMillis();
		
		try {
			// 判断是否为本机window开发环境,假定为window环境此处设定为dev环境变量
			String osname = System.getProperty("os.name");
			if(StringUtils.isNotEmpty(osname)&&osname.startsWith("Win")){
				System.setProperty("env", "dev");
			}
			
			// 读取log4j配置文件信息,使用绝对路径,保证不会出错
			String resourcePath = HouseCenterServerMain.class.getResource("/").getPath() + "log4j.properties";
			logger.info(resourcePath);
			PropertyConfigurator.configure(HouseCenterServerMain.class.getResource("/").getPath() + "log4j.properties");

			logger.info("House Center Server init.....");
			
			// 拉起spring服务主配置文件
			// 拉起spring-service.xml
			final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath*:conf-spring/spring-*.xml");
			
			HCConstants.context = context;
			
			if ("true".equals(System.getProperty(SHUTDOWN_HOOK_KEY))) {
			    Runtime.getRuntime().addShutdownHook(new Thread() {

			        @Override
			        public void run() {
			            try {
			                context.stop();
			                logger.warn("Server stopped.");
			            } catch (Exception e) {
			                logger.error(e.getMessage(), e);
			            } catch (Throwable t) {
			                logger.error(t.getMessage(), t);
			            }
			            synchronized (HouseCenterServerMain.class) {
			                running = false;
			                HouseCenterServerMain.class.notify();
			            }
			        }
			    });
			}
			
			// 拉起服务
			context.start();
			// 将配置加载到内存中
			HCConstants.getInstance().loadProperty();
			
			long end = System.currentTimeMillis();
			logger.info("House Center Server started, current env is [{}], take [{}] ms.", System.getProperty("env"),(end-start));
		} catch (BeansException e) {
			  logger.error(e.getMessage(), e);
			  e.printStackTrace();
	          System.exit(1);
		}

        synchronized (HouseCenterServerMain.class) {
            while (running) {
                try {
                	HouseCenterServerMain.class.wait();
                } catch (Throwable e) {
					logger.error(e.getMessage());
                }
            }
        }

        
	}
}
