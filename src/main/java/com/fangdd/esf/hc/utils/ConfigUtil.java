package com.fangdd.esf.hc.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 房源数据中心配置加载工具
 * @author tanhui@fangdd.com
 *
 */
public class ConfigUtil {

    private static Logger logger = LoggerFactory.getLogger(ConfigUtil.class);
    private static Properties properties;

    public static void loadProperties() {
        properties = loadProperties(HCConstants.getInstance().SYSTEM_PROPERTIES);
    }

    /**
     * load property from <b>classpath</b>
     * 
     * @return
     */
    public static Properties getProperties() {
        if (properties == null) {
            properties = loadProperties(HCConstants.getInstance().SYSTEM_PROPERTIES);
        }

        return properties;
    }

    public static String getProperty(String key) {
        return getProperties().getProperty(key, null);
    }

    public static String getProperty(String key, String defaultValue) {
        return getProperties().getProperty(key, defaultValue);
    }

    /**
     * @param fileName
     * @return
     */
    public static Properties loadProperties(String fileName) {
        Properties properties = new Properties();

        InputStream input = null;
        try {
            input = ConfigUtil.class.getClassLoader().getResourceAsStream(fileName);
            properties.load(input);
        } catch (IOException e) {
            logger.error("Failed to load " + fileName + " file from " + fileName + "(ingore this file): " + e.getMessage(), e);
            return null;
        } finally {
            try {
                if (input != null) {
                    logger.info("load " + fileName + " success.");
                    input.close();
                } else {
                    logger.error("load " + fileName + " failure.");
                }
            } catch (IOException e) {
                logger.error("Failed to load " + fileName + " file from " + fileName, e);
                return null;
            }
        }

        return properties;
    }
}
