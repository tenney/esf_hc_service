package com.fangdd.esf.hc.utils;

import org.springframework.context.ApplicationContext;

/**
 * 房源中心常量类
 * @author tanhui@fangdd.com
 *
 */
public class HCConstants {

    public static ApplicationContext context;

    /** Action在上下文key */
    public static final String CONTEX_ACTION_KEY = "CONTEX_ACTION_KEY";

    /** 请求报文在上下文key */
    public static final String REQ_MSG_KEY = "REQ_MSG_KEY";

    /** 返回报文在上下文key */
    public static final String RES_MSG_KEY = "RES_MSG_KEY";

    /** 返回码在上下文key */
    public static final String RES_CODE_KEY = "RES_CODE_KEY";

    /** ACC日志输出参数在上下文环境key */
    public static final String ACC_ARGS_KEY = "ACC_ARGS_KEY";

    // 经纪人绑定门店状态码
    /** 1 当前审核中 */
    public static final int AUDIT_BINDING = 1;
    /** 0 审核通过或无需审核，有效绑定状态 **/
    public static final int AUDIT_SUCCEED = 0;

    public void loadProperty() {
        ConfigUtil.loadProperties();
    }

    public String SYSTEM_PROPERTIES = "config_" + System.getProperty("env") + ".properties";
    private static HCConstants instance = new HCConstants();

    public static HCConstants getInstance() {
        return instance;
    }

    private HCConstants() {
    }

}
