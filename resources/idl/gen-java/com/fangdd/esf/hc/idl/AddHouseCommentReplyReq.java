/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.fangdd.esf.hc.idl;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddHouseCommentReplyReq implements org.apache.thrift.TBase<AddHouseCommentReplyReq, AddHouseCommentReplyReq._Fields>, java.io.Serializable, Cloneable, Comparable<AddHouseCommentReplyReq> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("AddHouseCommentReplyReq");

  private static final org.apache.thrift.protocol.TField COMMENT_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("commentId", org.apache.thrift.protocol.TType.I64, (short)1);
  private static final org.apache.thrift.protocol.TField CONTENT_FIELD_DESC = new org.apache.thrift.protocol.TField("content", org.apache.thrift.protocol.TType.STRING, (short)2);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new AddHouseCommentReplyReqStandardSchemeFactory());
    schemes.put(TupleScheme.class, new AddHouseCommentReplyReqTupleSchemeFactory());
  }

  public long commentId; // required
  public String content; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    COMMENT_ID((short)1, "commentId"),
    CONTENT((short)2, "content");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // COMMENT_ID
          return COMMENT_ID;
        case 2: // CONTENT
          return CONTENT;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __COMMENTID_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.COMMENT_ID, new org.apache.thrift.meta_data.FieldMetaData("commentId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    tmpMap.put(_Fields.CONTENT, new org.apache.thrift.meta_data.FieldMetaData("content", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(AddHouseCommentReplyReq.class, metaDataMap);
  }

  public AddHouseCommentReplyReq() {
  }

  public AddHouseCommentReplyReq(
    long commentId,
    String content)
  {
    this();
    this.commentId = commentId;
    setCommentIdIsSet(true);
    this.content = content;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public AddHouseCommentReplyReq(AddHouseCommentReplyReq other) {
    __isset_bitfield = other.__isset_bitfield;
    this.commentId = other.commentId;
    if (other.isSetContent()) {
      this.content = other.content;
    }
  }

  public AddHouseCommentReplyReq deepCopy() {
    return new AddHouseCommentReplyReq(this);
  }

  @Override
  public void clear() {
    setCommentIdIsSet(false);
    this.commentId = 0;
    this.content = null;
  }

  public long getCommentId() {
    return this.commentId;
  }

  public AddHouseCommentReplyReq setCommentId(long commentId) {
    this.commentId = commentId;
    setCommentIdIsSet(true);
    return this;
  }

  public void unsetCommentId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __COMMENTID_ISSET_ID);
  }

  /** Returns true if field commentId is set (has been assigned a value) and false otherwise */
  public boolean isSetCommentId() {
    return EncodingUtils.testBit(__isset_bitfield, __COMMENTID_ISSET_ID);
  }

  public void setCommentIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __COMMENTID_ISSET_ID, value);
  }

  public String getContent() {
    return this.content;
  }

  public AddHouseCommentReplyReq setContent(String content) {
    this.content = content;
    return this;
  }

  public void unsetContent() {
    this.content = null;
  }

  /** Returns true if field content is set (has been assigned a value) and false otherwise */
  public boolean isSetContent() {
    return this.content != null;
  }

  public void setContentIsSet(boolean value) {
    if (!value) {
      this.content = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case COMMENT_ID:
      if (value == null) {
        unsetCommentId();
      } else {
        setCommentId((Long)value);
      }
      break;

    case CONTENT:
      if (value == null) {
        unsetContent();
      } else {
        setContent((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case COMMENT_ID:
      return Long.valueOf(getCommentId());

    case CONTENT:
      return getContent();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case COMMENT_ID:
      return isSetCommentId();
    case CONTENT:
      return isSetContent();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof AddHouseCommentReplyReq)
      return this.equals((AddHouseCommentReplyReq)that);
    return false;
  }

  public boolean equals(AddHouseCommentReplyReq that) {
    if (that == null)
      return false;

    boolean this_present_commentId = true;
    boolean that_present_commentId = true;
    if (this_present_commentId || that_present_commentId) {
      if (!(this_present_commentId && that_present_commentId))
        return false;
      if (this.commentId != that.commentId)
        return false;
    }

    boolean this_present_content = true && this.isSetContent();
    boolean that_present_content = true && that.isSetContent();
    if (this_present_content || that_present_content) {
      if (!(this_present_content && that_present_content))
        return false;
      if (!this.content.equals(that.content))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  @Override
  public int compareTo(AddHouseCommentReplyReq other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetCommentId()).compareTo(other.isSetCommentId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCommentId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.commentId, other.commentId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetContent()).compareTo(other.isSetContent());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetContent()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.content, other.content);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("AddHouseCommentReplyReq(");
    boolean first = true;

    sb.append("commentId:");
    sb.append(this.commentId);
    first = false;
    if (!first) sb.append(", ");
    sb.append("content:");
    if (this.content == null) {
      sb.append("null");
    } else {
      sb.append(this.content);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws TException {
    // check for required fields
    // alas, we cannot check 'commentId' because it's a primitive and you chose the non-beans generator.
    if (content == null) {
      throw new TProtocolException("Required field 'content' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class AddHouseCommentReplyReqStandardSchemeFactory implements SchemeFactory {
    public AddHouseCommentReplyReqStandardScheme getScheme() {
      return new AddHouseCommentReplyReqStandardScheme();
    }
  }

  private static class AddHouseCommentReplyReqStandardScheme extends StandardScheme<AddHouseCommentReplyReq> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, AddHouseCommentReplyReq struct) throws TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // COMMENT_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.commentId = iprot.readI64();
              struct.setCommentIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // CONTENT
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.content = iprot.readString();
              struct.setContentIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetCommentId()) {
        throw new TProtocolException("Required field 'commentId' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, AddHouseCommentReplyReq struct) throws TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(COMMENT_ID_FIELD_DESC);
      oprot.writeI64(struct.commentId);
      oprot.writeFieldEnd();
      if (struct.content != null) {
        oprot.writeFieldBegin(CONTENT_FIELD_DESC);
        oprot.writeString(struct.content);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class AddHouseCommentReplyReqTupleSchemeFactory implements SchemeFactory {
    public AddHouseCommentReplyReqTupleScheme getScheme() {
      return new AddHouseCommentReplyReqTupleScheme();
    }
  }

  private static class AddHouseCommentReplyReqTupleScheme extends TupleScheme<AddHouseCommentReplyReq> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, AddHouseCommentReplyReq struct) throws TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeI64(struct.commentId);
      oprot.writeString(struct.content);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, AddHouseCommentReplyReq struct) throws TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.commentId = iprot.readI64();
      struct.setCommentIdIsSet(true);
      struct.content = iprot.readString();
      struct.setContentIsSet(true);
    }
  }

}

